using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class ScriptEliminarEspacio : MonoBehaviour
{
    public Image piso;
    public Sprite escenario1;
    public Sprite escenario2;
    public Sprite escenario3;
    public Sprite noImage;

    public Dropdown DrpPisoEliminar;

    public class Pisos
    {
        public bool[] EnUso;
    }

    public class Escenario
    {
        public bool Status;
        public int Iluminacion;
        public int Escritorio;
        public int Silla;
        public int Cuadro;
    }

    // Start is called before the first frame update
    void Start()
    {
        ActualizarOpciones();
        ActualizarEscenario();
    }

    // Update is called once per frame
    void Update()
    {
        // ActualizarOpciones();
    }

    // Regresar al Menu Inicio
    public void RegresarMenuInicio()
    {
        SceneManager.LoadScene("MenuInicio");
    }

    // Alerta con mensaje
    public void MsgAlerta(string msg)
    {
        Toast.Instance.Show(msg, 2f, Toast.ToastColor.Blue);
    }

    public void EliminarEspacio()
    {
        Pisos mispisos = new Pisos();
        string pathEscenarios = Application.dataPath + "/Escenarios.json";
        string json = System.IO.File.ReadAllText(pathEscenarios);
        JsonUtility.FromJsonOverwrite(json, mispisos);

        // Mapeando index de la opcion disponible
        string pisoSelec = DrpPisoEliminar.captionText.text;
        int index = -1;
        if (pisoSelec.Equals("Piso Mario Kart")) index = 0;
        else if (pisoSelec.Equals("Piso Mario Odyssey")) index = 1;
        else if (pisoSelec.Equals("Piso Super Smash")) index = 2;

        if (index != -1)
        {
            // Escribiendo en la bitacora la accion
            string rutaBitacora = Application.dataPath + "/bitacora_201503840_201503925.txt";
            if (File.Exists(rutaBitacora))
            {
                File.AppendAllText(rutaBitacora, "[ACCION] Usuario eliminó el Espacio#" + (index + 1).ToString() + "\n");
            }

            // Modificando el archivo de escenarios
            if (File.Exists(pathEscenarios))
            {
                mispisos.EnUso[index] = false;
                string jsonMod = JsonUtility.ToJson(mispisos, true);
                System.IO.File.WriteAllText(pathEscenarios, jsonMod);
            }

            // Modificando el archivo de la posicion de Escenario
            string rutaEscenarioActual = Application.dataPath + "/Escenario" + (index + 1).ToString() + ".json";
            if (File.Exists(rutaEscenarioActual))
            {
                Escenario escenario = new Escenario();
                escenario.Status = false;
                escenario.Iluminacion = -1;
                escenario.Escritorio = -1;
                escenario.Silla = -1;
                escenario.Cuadro = -1;

                string jsonMod = JsonUtility.ToJson(escenario, true);
                System.IO.File.WriteAllText(rutaEscenarioActual, jsonMod);
            }
            // Actualiznado opciones graficamente y imprimiendo alerta
            ActualizarOpciones();
            ActualizarEscenario();
            MsgAlerta("Espacio#" + (index + 1).ToString() + " eliminado! :(");
        }
        else
        {
            // Piso ya ocupado ERROR
            MsgAlerta("ERROR> Sin espacios para eliminar!");
            // Escribiendo en la bitacora la accion
            string rutaBitacora = Application.dataPath + "/bitacora_201503840_201503925.txt";
            if (File.Exists(rutaBitacora))
            {
                File.AppendAllText(rutaBitacora, "[ERROR] Sin espacios para eliminar.\n");
            }
        }
    }

    public void ActualizarOpciones()
    {
        // Agregando pisos disponibles al inicio
        Pisos mispisos = new Pisos();
        List<string> pisosDisponibles = new List<string>();
        string path = Application.dataPath + "/Escenarios.json";
        string json = System.IO.File.ReadAllText(path);
        JsonUtility.FromJsonOverwrite(json, mispisos);

        if (mispisos.EnUso[0]) pisosDisponibles.Add("Piso Mario Kart");
        if (mispisos.EnUso[1]) pisosDisponibles.Add("Piso Mario Odyssey");
        if (mispisos.EnUso[2]) pisosDisponibles.Add("Piso Super Smash");

        // Limpiando las opciones
        DrpPisoEliminar.ClearOptions();
        // Agregando los pisos disponibles
        DrpPisoEliminar.AddOptions(pisosDisponibles);
    }

    public void ActualizarEscenario()
    {
        // Mapeando index de la opcion disponible
        string pisoSelec = DrpPisoEliminar.captionText.text;
        int index = -1;
        if (pisoSelec.Equals("Piso Mario Kart")) index = 0;
        else if (pisoSelec.Equals("Piso Mario Odyssey")) index = 1;
        else if (pisoSelec.Equals("Piso Super Smash")) index = 2;

        if (index == 0) piso.sprite = escenario1;
        else if (index == 1) piso.sprite = escenario2;
        else if (index == 2) piso.sprite = escenario3;
        else piso.sprite = noImage;
    }
}
