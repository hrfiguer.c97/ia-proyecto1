using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class ScriptAR : MonoBehaviour
{
    public GameObject Escritorio1;
    public GameObject Escritorio2;
    public GameObject Escritorio3;

    public GameObject Cuadro1;
    public GameObject Cuadro2;
    public GameObject Cuadro3;

    public GameObject Silla1;
    public GameObject Silla2;
    public GameObject Silla3;
    public Light luzJuego;
    public Vector3 Centro = new Vector3(0f, 0f, 0f);
    public Vector3 SuperiorDerecha = new Vector3(6.5f, 0.5f, 3.5f);
    public Vector3 InferiorDerecha = new Vector3(6.5f, 0.5f, -4f);
    public Vector3 InferiorIzquierda = new Vector3(-6.8f, 0.5f, -4f);
    public Vector3 SuperiorIzquierda = new Vector3(-6.8f, 0.5f, 3.5f);
    public Vector3 Pared1 = new Vector3(0.4f, 1.45f, 5.25f);
    public Vector3 Pared2 = new Vector3(9.4f, 1.45f, 0f);
    public Quaternion Pared2Rot = new Quaternion(0f, 0.707106829f, 0f, 0.707106829f);

    public class Escenario
    {
        public bool Status;
        public int Iluminacion;
        public int Escritorio;
        public int Silla;
        public int Cuadro;
    }

    // Start is called before the first frame update
    void Start()
    {
        CargarEscenarios();
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Regresar al Menu Inicio
    public void RegresarMenuInicio()
    {
        SceneManager.LoadScene("MenuInicio");
    }

    public void CargarEscenarios()
    {
        // Recuperando informacion de los Escenarios
        Escenario escenario1 = new Escenario();
        Escenario escenario2 = new Escenario();
        Escenario escenario3 = new Escenario();
        string pathRelativa = Application.dataPath;
        string json = System.IO.File.ReadAllText(pathRelativa + "/Escenario1.json");
        JsonUtility.FromJsonOverwrite(json, escenario1);
        json = System.IO.File.ReadAllText(pathRelativa + "/Escenario2.json");
        JsonUtility.FromJsonOverwrite(json, escenario2);
        json = System.IO.File.ReadAllText(pathRelativa + "/Escenario3.json");
        JsonUtility.FromJsonOverwrite(json, escenario3);

        if (escenario1.Status)
        {
            PosicionarEscritorio(escenario1.Escritorio, 1);
            PosicionarSilla(escenario1.Silla, 1);
            PosicionarCuadro(escenario1.Cuadro, 1);
        }
        else
        {
            // Ocultando objetos del Escenario1
            Escritorio1.SetActive(false);
            Cuadro1.SetActive(false);
            Silla1.SetActive(false);
        }

        if (escenario2.Status)
        {
            PosicionarEscritorio(escenario2.Escritorio, 2);
            PosicionarSilla(escenario2.Silla, 2);
            PosicionarCuadro(escenario2.Cuadro, 2);
        }
        else
        {
            // Ocultando objetos del Escenario2
            Escritorio2.SetActive(false);
            Cuadro2.SetActive(false);
            Silla2.SetActive(false);
        }

        if (escenario3.Status)
        {
            PosicionarEscritorio(escenario3.Escritorio, 3);
            PosicionarSilla(escenario3.Silla, 3);
            PosicionarCuadro(escenario3.Cuadro, 3);
        }
        else
        {
            // Ocultando objetos del Escenario3
            Escritorio3.SetActive(false);
            Cuadro3.SetActive(false);
            Silla3.SetActive(false);
        }
    }

    public void PosicionarEscritorio(int posicion, int escenario)
    {
        switch (posicion)
        {
            case 0: // Superior Izquierda
                if (escenario == 1) Escritorio1.transform.position = SuperiorIzquierda;
                else if (escenario == 2) Escritorio2.transform.position = SuperiorIzquierda;
                else if (escenario == 3) Escritorio3.transform.position = SuperiorIzquierda;
                break;
            case 1: // Superior Derecha
                if (escenario == 1) Escritorio1.transform.position = SuperiorDerecha;
                else if (escenario == 2) Escritorio2.transform.position = SuperiorDerecha;
                else if (escenario == 3) Escritorio3.transform.position = SuperiorDerecha;
                break;
            case 2: // Centro
                if (escenario == 1) Escritorio1.transform.position = Centro;
                else if (escenario == 2) Escritorio2.transform.position = Centro;
                else if (escenario == 3) Escritorio3.transform.position = Centro;
                break;
            case 3: // Inferior Izquierda
                if (escenario == 1) Escritorio1.transform.position = InferiorIzquierda;
                else if (escenario == 2) Escritorio2.transform.position = InferiorIzquierda;
                else if (escenario == 3) Escritorio3.transform.position = InferiorIzquierda;
                break;
            case 4: // Inferior Derecha
                if (escenario == 1) Escritorio1.transform.position = InferiorDerecha;
                else if (escenario == 2) Escritorio2.transform.position = InferiorDerecha;
                else if (escenario == 3) Escritorio3.transform.position = InferiorDerecha;
                break;
        }
    }

    public void PosicionarSilla(int posicion, int escenario)
    {
        switch (posicion)
        {
            case 0: // Superior Izquierda
                if (escenario == 1) Silla1.transform.position = SuperiorIzquierda;
                else if (escenario == 2) Silla2.transform.position = SuperiorIzquierda;
                else if (escenario == 3) Silla3.transform.position = SuperiorIzquierda;
                break;
            case 1: // Superior Derecha
                if (escenario == 1) Silla1.transform.position = SuperiorDerecha;
                else if (escenario == 2) Silla2.transform.position = SuperiorDerecha;
                else if (escenario == 3) Silla3.transform.position = SuperiorDerecha;
                break;
            case 2: // Centro
                if (escenario == 1) Silla1.transform.position = Centro;
                else if (escenario == 2) Silla2.transform.position = Centro;
                else if (escenario == 3) Silla3.transform.position = Centro;
                break;
            case 3: // Inferior Izquierda
                if (escenario == 1) Silla1.transform.position = InferiorIzquierda;
                else if (escenario == 2) Silla2.transform.position = InferiorIzquierda;
                else if (escenario == 3) Silla3.transform.position = InferiorIzquierda;
                break;
            case 4: // Inferior Derecha
                if (escenario == 1) Silla1.transform.position = InferiorDerecha;
                else if (escenario == 2) Silla2.transform.position = InferiorDerecha;
                else if (escenario == 3) Silla3.transform.position = InferiorDerecha;
                break;
        }
    }

    public void PosicionarCuadro(int posicion, int escenario)
    {
        switch (posicion)
        {
            case 0: // Pared 1
                if (escenario == 1) Cuadro1.transform.position = Pared1;
                else if (escenario == 2) Cuadro2.transform.position = Pared1;
                else if (escenario == 3) Cuadro3.transform.position = Pared1;
                break;
            case 1: // Pared 2
                if (escenario == 1) { Cuadro1.transform.position = Pared2; Cuadro1.transform.rotation = Pared2Rot; }
                else if (escenario == 2) { Cuadro2.transform.position = Pared2; Cuadro2.transform.rotation = Pared2Rot; }
                else if (escenario == 3) { Cuadro3.transform.position = Pared2; Cuadro3.transform.rotation = Pared2Rot; }
                break;
        }
    }

    public void CambiarColorEscenario(int escenario)
    {
        // Recuperando informacion de los Escenarios
        Escenario escenarioActual = new Escenario();
        string pathRelativa = Application.dataPath;
        string json = System.IO.File.ReadAllText(pathRelativa + "/Escenario" + escenario.ToString() + ".json");
        JsonUtility.FromJsonOverwrite(json, escenarioActual);

        switch (escenarioActual.Iluminacion)
        {
            case 0: // Rojo
                luzJuego.color = Color.red;
                break;
            case 1: // Azul
                luzJuego.color = Color.blue;
                break;
            case 2: // Verde
                luzJuego.color = Color.green;
                break;
            default:
                luzJuego.color = Color.white;
                break;
        }
    }
}
