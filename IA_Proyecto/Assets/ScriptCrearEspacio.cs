using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class ScriptCrearEspacio : MonoBehaviour
{
    public Dropdown DrpPiso;
    public Dropdown DrpIluminacion;
    public Dropdown DrpEscritorio;
    public Dropdown DrpSilla;
    public Dropdown DrpCuadro;

    public class Pisos
    {
        public bool[] EnUso;
    }

    public class Escenario
    {
        public bool Status;
        public int Iluminacion;
        public int Escritorio;
        public int Silla;
        public int Cuadro;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    // Regresar al Menu Inicio
    public void RegresarMenuInicio()
    {
        SceneManager.LoadScene("MenuInicio");
    }

    // Alerta con mensaje de espacio creado
    public void MsgAlerta(string message)
    {
        Toast.Instance.Show(message, 2f, Toast.ToastColor.Blue);
    }

    public void CrearEspacio()
    {
        Pisos mispisos = new Pisos();
        string pathEscenarios = Application.dataPath + "/Escenarios.json";
        string json = System.IO.File.ReadAllText(pathEscenarios);
        JsonUtility.FromJsonOverwrite(json, mispisos);

        int pisoSelec = DrpPiso.value;

        if (!mispisos.EnUso[pisoSelec])
        {
            // Piso disponible para crear
            // Escribiendo en la bitacora la accion
            string rutaBitacora = Application.dataPath + "/bitacora_201503840_201503925.txt";
            if (File.Exists(rutaBitacora))
            {
                File.AppendAllText(rutaBitacora, "[ACCION] Usuario creó el Espacio#" + (pisoSelec + 1).ToString() + ".\n");
            }

            // Modificando el archivo de escenarios
            if (File.Exists(pathEscenarios))
            {
                mispisos.EnUso[pisoSelec] = true;
                string jsonMod = JsonUtility.ToJson(mispisos, true);
                System.IO.File.WriteAllText(pathEscenarios, jsonMod);
            }

            // Modificando el archivo de la posicion de Escenario
            string rutaEscenarioActual = Application.dataPath + "/Escenario" + (pisoSelec + 1).ToString() + ".json";
            if (File.Exists(rutaEscenarioActual))
            {
                Escenario escenario = new Escenario();
                escenario.Status = true;
                escenario.Iluminacion = DrpIluminacion.value;
                escenario.Escritorio = DrpEscritorio.value;
                escenario.Silla = DrpSilla.value;
                escenario.Cuadro = DrpCuadro.value;

                string jsonMod = JsonUtility.ToJson(escenario, true);
                System.IO.File.WriteAllText(rutaEscenarioActual, jsonMod);
            }

            // Imprimiando alerta
            MsgAlerta("Espacio#" + (pisoSelec + 1).ToString() + " creado! :)");
        }
        else
        {
            // Piso ya ocupado ERROR
            MsgAlerta("ERROR> Espacio#" + (pisoSelec + 1).ToString() + " ya creado!");
            // Escribiendo en la bitacora la accion
            string rutaBitacora = Application.dataPath + "/bitacora_201503840_201503925.txt";
            if (File.Exists(rutaBitacora))
            {
                File.AppendAllText(rutaBitacora, "[ERROR] Espacio#" + (pisoSelec + 1).ToString() + " ya fue creado.\n");
            }
        }
    }
}
