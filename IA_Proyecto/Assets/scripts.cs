using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;

public class scripts : MonoBehaviour
{
    // public Image imagen;
    // public Sprite escenario1;
    // public Sprite escenario2;
    // public Sprite escenario3;

    // Start is called before the first frame update
    void Start()
    {
        string ruta = Application.dataPath + "/bitacora_201503840_201503925.txt";
        if (!File.Exists(ruta))
        {
            File.WriteAllText(ruta, "Bitacora: \n\n");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Mostrar menu de Crear Espacio
    public void ShowCrearEspacio()
    {
        SceneManager.LoadScene("CrearEspacio");
    }

    // Mostrar Espacio en AR
    public void ShowVerEspacioAR()
    {
        SceneManager.LoadScene("VerEspacioAR");
    }

    // Mostrar menu Eliminar Espacio
    public void ShowEliminarEspacio()
    {
        SceneManager.LoadScene("EliminarEspacio");
    }
}
